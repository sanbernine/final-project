<?php

namespace App\Http\Controllers;

use App\Model\QComment;
use Auth;
use Illuminate\Http\Request;

class QCommentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }
    
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $qcomments = QComment::where('answer', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $qcomments = QComment::latest()->paginate($perPage);
        }

        return view('q-comments.index', compact('qcomments'));
    }

    public function create($questionId)
    {
        return view('q-comments.create', compact('questionId'));
    }

    public function store(Request $request, $questionId)
    {
        $comment = new QComment();
        $comment->user_id = Auth::user()->id;
        $comment->question_id = $questionId;
        $comment->comment = $request->comment;
        $comment->save();
        
        return redirect()
            ->route('questions.edit', $questionId)
            ->with('flash_message', 'Comment added!');
    }

    public function show($questionId, $commentId)
    {
        $comment = QComment::findOrFail($commentId);

        return view('q-comments.show', compact('comment'));
    }

    public function edit($questionId, $commentId)
    {
        $comment = QComment::findOrFail($commentId);

        return view('q-comments.edit', compact('questionId', 'comment'));
    }

    public function update(Request $request, $questionId, $commentId)
    {
        $comment = QComment::findOrFail($commentId);
        $comment->comment = $request->comment;
        $comment->update();

        return redirect()
            ->route('questions.show', $questionId)
            ->with('flash_message', 'Comment updated!');
    }

    public function destroy($questionId, $commentId)
    {
        QComment::destroy($commentId);

        return redirect()
            ->route('questions.show', $questionId)
            ->with('flash_message', 'Comment deleted!');
    }
}
