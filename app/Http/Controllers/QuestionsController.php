<?php

namespace App\Http\Controllers;

use App\Model\Answer;
use App\Model\QComment;
use App\Model\AComment;
use App\Model\Question;
use App\Model\Tag;
use Auth;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show', 'edit');
    }
    
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $questions = Question::where('title', 'LIKE', "%$keyword%")
                ->orWhere('question', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $questions = Question::latest()->paginate($perPage);
        }

        return view('questions.index', compact('questions'));
    }

    public function create()
    {
        return view('questions.create');
    }

    public function store(Request $request)
    {
        $question = new Question();
        $question->user_id = Auth::user()->id;
        $question->title = $request->title;
        $question->question = $request->question;
        $question->save();

        $requestTags = explode(",", $request->tags);

        foreach($requestTags as $requestTag) {
            $tag = Tag::firstOrCreate(['tag' => $requestTag]);
            $question->tags()->attach($tag->id);
        }
        
        return redirect()
            ->route('questions.index')
            ->with("berhasil","Kamu Berhasil Menambah");

    }

    public function show($questionId)
    {
        $question = Question::findOrFail($questionId);

        $perPage = 25;

        $answers = Answer::where('question_id', $questionId)
            ->latest()->paginate($perPage);
     
        $qcomments = QComment::where('question_id', $questionId)
            ->latest()->paginate($perPage);

        $acomments = AComment::where('question_id', $questionId)
            ->latest()->paginate($perPage);
        return view('questions.show', compact('question', 'answers', 'qcomments'));
    }

    public function edit($id)
    {
        $question = Question::findOrFail($id);

        $tags = array();

        foreach ($question->tags as $tag) {
            $tags[] = $tag->tag;
        }

        $question->tags = implode(',', $tags);

        $perPage = 25;

        $answers = Answer::where('question_id', $id)
            ->latest()->paginate($perPage);
     
        $qcomments = QComment::where('question_id', $id)
            ->latest()->paginate($perPage);

        return view('questions.edit', compact('question', 'answers', 'qcomments'));
    }

    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        
        $question = Question::findOrFail($id);
        $question->update($requestData);

        return redirect('questions')->with('flash_message', 'Question updated!');
    }

    public function vote($id, $vote)
    {
        $question = Question::findOrFail($id);
        $question->votes()->sync([Auth::user()->id => ['vote' => $vote]]);

        return back()->with('flash_message', 'Rate applied!');
    }

    public function destroy($id)
    {
        Question::destroy($id);

        return redirect('questions')->with('flash_message', 'Question deleted!');
    }
}
