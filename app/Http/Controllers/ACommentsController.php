<?php

namespace App\Http\Controllers;

use App\Model\AComment;
use Auth;
use Illuminate\Http\Request;

class ACommentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $acomments = AComment::where('answer', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $acomments = AComment::latest()->paginate($perPage);
        }

        return view('a-comments.index', compact('acomments'));
        
    }
    
    public function create($questionId, $answerId)
    {
        return view('a-comments.create', compact('questionId', 'answerId'));
    }

    public function store(Request $request, $questionId, $answerId)
    {
        $comment = new AComment();
        $comment->user_id = Auth::user()->id;
        $comment->answer_id = $answerId;
        $comment->comment = $request->comment;
        $comment->save();

        return redirect()
            ->route('questions.edit', ['question' => $questionId, 'answer' => $answerId])
            ->with('flash_message', 'Comment added!');
    }

    public function show($questionId, $answerId, $commentId)
    {
        $comment = AComment::findOrFail($commentId);

        return view('a-comments.show', compact('questionId', 'answerId', 'comment'));
    }

    public function edit($questionId, $answerId, $commentId)
    {
        $comment = AComment::findOrFail($commentId);

        return view('a-comments.edit', compact('questionId', 'answerId', 'comment'));
    }

    public function update(Request $request, $questionId, $answerId, $commentId)
    {
        $comment = AComment::findOrFail($commentId);
        $comment->comment = $request->comment;
        $comment->update();

        return redirect()
            ->route('answers.show', ['question' => $questionId, 'answer' => $answerId])
            ->with('flash_message', 'Comment updated!');
    }

    public function destroy($questionId, $answerId, $commentId)
    {
        AComment::destroy($commentId);

        return redirect()
            ->route('answers.show', ['question' => $questionId, 'answer' => $answerId])
            ->with('flash_message', 'Comment deleted!');
    }
}
