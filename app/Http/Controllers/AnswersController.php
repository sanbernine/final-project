<?php

namespace App\Http\Controllers;

use App\Model\AComment;
use App\Model\Answer;
use Auth;
use Illuminate\Http\Request;

class AnswersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }
    
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $answers = Answer::where('answer', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $answers = Answer::latest()->paginate($perPage);
        }

        return view('answers.index', compact('answers'));
    }

    public function create($questionId)
    {
        return view('answers.create', compact('questionId'));
    }

    public function store(Request $request, $questionId)
    {
        $answer = new Answer();
        $answer->user_id = Auth::user()->id;
        $answer->question_id = $questionId;
        $answer->answer = $request->answer;
        $answer->save();
        
        return redirect()
            ->route('questions.edit', $questionId)
            ->with('flash_message', 'Answer added!');
    }

    public function show($questionId, $answerId)
    {
        $answer = Answer::findOrFail($answerId);

        $perPage = 25;
     
        $acomments = AComment::where('answer_id', $answerId)
            ->latest()->paginate($perPage);

        return view('answers.show', compact('questionId', 'answer', 'acomments'));
    }

    public function edit($questionId, $answerId)
    {
        $answer = Answer::findOrFail($answerId);

        return view('answers.edit', compact('questionId', 'answer'));
    }

    public function update(Request $request, $questionId, $answerId)
    {
        $requestData = $request->all();
        
        $answer = Answer::findOrFail($answerId);
        $answer->update($requestData);

        return redirect()
            ->route('questions.show', $questionId)
            ->with('flash_message', 'Answer updated!');
    }

    public function accept($questionId, $answerId)
    {
        $answers = Answer::where('question_id', $questionId)->get();

        foreach($answers as $answer) {
            $answer->is_accepted = false;
            $answer->update();
        }

        $answer = Answer::findOrFail($answerId);
        $answer->is_accepted = true;
        $answer->update();

        return redirect()
            ->back()
            ->with('flash_message', 'Answer updated!');
    }

    public function vote($questionId, $answerId, $vote)
    {
        $answer = Answer::findOrFail($answerId);
        $answer->votes()->sync([Auth::user()->id => ['vote' => $vote]]);

        return back()->with('flash_message', 'Rate applied!');
    }

    public function destroy($questionId, $answerId)
    {
        Answer::destroy($answerId);

        return back()->with('flash_message', 'Answer deleted!');
    }
}
