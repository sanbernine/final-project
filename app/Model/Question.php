<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['title', 'question'];

    public function answers()
    {
        return $this->hasMany('App\Model\Answer');
    }

    public function comment()
    {
        return $this->hasMany('App\Model\AComment');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Model\Tag');
    }

    public function votes()
    {
        return $this->belongsToMany('App\Model\User', 'q_poins');
    }
}
