<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['tag'];
    
    public function questions() {
        return $this->belongsToMany('App\Model\Question');
    }
}
