<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QComment extends Model
{
    protected $fillable = ['comment'];

    public function questions()
    {
        return $this->belongsTo('App\Model\Question');
    }
}
