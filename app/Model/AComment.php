<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AComment extends Model
{
    protected $fillable = ['comment'];

    public function answers()
    {
        return $this->belongsTo('App\Model\Answer');
    }
}
