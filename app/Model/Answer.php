<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = ['answer'];

    public function questions()
    {
        return $this->belongsTo('App\Model\Question');
    }

    public function comment()
    {
        return $this->hasMany('App\Model\AComment');
    }

    public function votes()
    {
        return $this->belongsToMany('App\Model\User', 'a_poins');
    }
}
