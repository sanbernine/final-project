<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'QuestionsController@index');

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');
Route::get('homeuser', 'HomeController@index1')->name('homeuser1');

Route::resource('questions', 'QuestionsController');

Route::group(['prefix' => 'questions/{question}'], function () {
    Route::post('vote/{vote}', 'QuestionsController@vote')->name('questions.vote');
    Route::resource('answers', 'AnswersController');
    Route::resource('comments', 'QCommentsController', ['as' => 'question']);

    Route::group(['prefix' => 'answers/{answer}'], function () {
        Route::post('accept', 'AnswersController@accept')->name('answers.accept');
        Route::post('vote/{vote}', 'AnswersController@vote')->name('answers.vote');
        Route::resource('comments', 'ACommentsController', ['as' => 'answer']);
    });
});
