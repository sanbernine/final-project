@extends('layouts.coba')


@section('content')

<div class="question-summary" id="question-summary-60427542">
    <div class="statscontainer">
        <div class="stats">
            <div class="vote">
                <div class="votes">
                    <span class="vote-count-post "><strong>-1</strong></span>
                    <div class="viewcount">votes</div>
                </div>
            </div>
            <!-- answer -->
            <div class="status answered">
                <strong>2</strong>answers
            </div>
            <!-- answer -->
        </div>
<!-- view -->
<div class="views " title="20 views">
    20 views
</div>
<!-- /view -->
    </div>

    <div class="summary">
        <!-- judul dan kelink -->
        <h3><a href="/questions/60427542/want-to-create-a-pdf-viewer-in-laravel" class="question-hyperlink">Want to create a pdf viewer in Laravel [closed]</a></h3>
        <!-- /judul kelink -->

            <!-- isi  -->
        <div class="excerpt">
            I working on a application where i need to create a pdf viewer which is meant to that particular application using php Laravel .
I have refereed  this https://github.com/goodnesskay/LARAVEL-PDF-VIEWER
...
        </div>
        <!-- /isi -->

        <!-- tampil tag rooting berdasarkan tags dari id_pertanyaan -->
        <div class="tags t-php t-laravel t-pdf t-pdf-viewer">
            <a href="/questions/tagged/php" class="post-tag" title="show questions tagged &#39;php&#39;" rel="tag">php</a> <a href="/questions/tagged/laravel" class="post-tag" title="show questions tagged &#39;laravel&#39;" rel="tag">laravel</a> <a href="/questions/tagged/pdf" class="post-tag" title="show questions tagged &#39;pdf&#39;" rel="tag">pdf</a> <a href="/questions/tagged/pdf-viewer" class="post-tag" title="show questions tagged &#39;pdf-viewer&#39;" rel="tag">pdf-viewer</a> 
        </div>
        <!-- tampil tag rooting -->

        <div class="started fr">
            <div class="user-info ">
    <div class="user-action-time">
        asked <span title="2020-02-27 06:40:25Z" class="relativetime">2 hours ago</span>
    </div>
    <div class="user-gravatar32">
        <a href="/users/10451451/mahesh-kalasa"><div class="gravatar-wrapper-32"><img src="https://lh4.googleusercontent.com/-TKnIY3SMzpc/AAAAAAAAAAI/AAAAAAAABmM/-Ne9MPmUKtQ/photo.jpg?sz=32" alt="" width="32" height="32" class="bar-sm"></div></a>
    </div>
    <div class="user-details">
        <a href="/users/10451451/mahesh-kalasa">Mahesh Kalasa</a>
        <div class="-flair">
            <span class="reputation-score" title="reputation score " dir="ltr">1</span>
        </div>
    </div>
</div>
        </div>
    </div>


</div>



@endsection

<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });

/*@if (session("berhasil")) {}*/
    Swal.fire({
        title: 'Berhasil!',
        // text: "{{session('berhasil')}}",
        text: 'berhasil',
        icon: 'success',
        confirmButtonText: 'Cool'
    })
/*@endif*/
</script>

<div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif