<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Sidebar
        </div>

        <div class="card-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ route('questions.index') }}">
                        Question
                    </a>
                </li>
            </ul>
        </div>

        <div class="card-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ route('answers.index', 1) }}">
                        Answer
                    </a>
                </li>
            </ul>
        </div>

        <div class="card-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ route('question.comments.index', 1) }}">
                        Q Comment
                    </a>
                </li>
            </ul>
        </div>

        <div class="card-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ route('answer.comments.index', ['question' => 1, 'answer' => 1]) }}">
                        A Comment
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
