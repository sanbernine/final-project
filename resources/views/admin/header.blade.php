<header class="top-bar js-top-bar top-bar__network _fixed">
    <div class="wmx12 mx-auto grid ai-center h100" role="menubar">
        <div class="-main grid--cell">
                <a href="#" class="left-sidebar-toggle p0 ai-center jc-center js-left-sidebar-toggle" role="menuitem" aria-haspopup="true" aria-controls="left-sidebar" aria-expanded="false"><span class="ps-relative"></span></a>
                <div class="topbar-dialog leftnav-dialog js-leftnav-dialog dno">
                    <div class="left-sidebar js-unpinned-left-sidebar" data-can-be="left-sidebar" data-is-here-when="sm"></div>
                </div>
                                <a href="{{route('questions.create')}}" class="-logo js-gps-track"
                        data-gps-track="top_nav.click({is_current:false, location:5, destination:8})">
                        <span class="-img _glyph">Stack Overflow</span>
                    </a>
        </div>
        <ol class="overflow-x-auto ml-auto -secondary grid ai-center list-reset h100 user-logged-out" role="presentation">
                <li class="-item searchbar-trigger"><a href="#" class="-link js-searchbar-trigger" role="button" aria-label="Search" aria-haspopup="true" aria-controls="search" title="Click to show search"><svg aria-hidden="true" class="svg-icon iconSearch" width="18" height="18" viewBox="0 0 18 18"><path d="M18 16.5l-5.14-5.18h-.35a7 7 0 1 0-1.19 1.19v.35L16.5 18l1.5-1.5zM12 7A5 5 0 1 1 2 7a5 5 0 0 1 10 0z"/></svg></a></li>
                
    

                @if(Auth::check())
   
                    
                    <li>
                            <a onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out fa-fw"></i> Logout</a>
                                     
                        </li>

                     <form id="logout-form" action="{{ route('logout')}} " method="post" style="display: none;">
                                          @csrf
                    </form>
@else
    <li class="grid--cell md:d-none">
                        <a href="{{route('register')}}" class="-marketing-link js-gps-track"
                           data-gps-track="top_nav.products.click({location:5, destination:7})"
                            >Register</a>

                            <a href="{{route('login')}}" class="-marketing-link js-gps-track"
                           data-gps-track="top_nav.products.click({location:5, destination:7})"
                            >Login</a>
                    </li>
@endif



                    
            <li class="js-topbar-dialog-corral" role="presentation">

            </li>
        </ol>

    </div>
</header>