<!DOCTYPE html>

    <html class="html__responsive">

    <head>

        <title>Project</title>
        <link rel="stylesheet" type="text/css" href="https://cdn.sstatic.net/Shared/stacks.css?v=f6dc2e55f461" >
        <link rel="stylesheet" type="text/css" href="https://cdn.sstatic.net/Sites/stackoverflow/primary.css?v=fe46cf48500c" >
        
    
    </head>

            
@include('user/header')
    <div class="container">

<div id="left-sidebar" data-is-here-when="md lg" class="left-sidebar js-pinned-left-sidebar ps-relative">
    <div class="left-sidebar--sticky-container js-sticky-leftnav">
        <nav role="navigation">
            <ol class="nav-links">
    
                @include('user.navkiri')          
            </ol>
        </nav>
    </div>   
</div>
        

<div id="content" class="snippet-hidden"> 
<div id="mainbar">
<div data-controller="se-uql" data-se-uql-id="" data-se-uql-sanitize-tag-query="false">
    <div class="grid ai-center jc-space-between mb12 sm:fd-column sm:ai-stretch">
            
    </div>
    <div class="grid">
            <h1 class="grid--cell fl1 fs-headline1">
                    Create Question
            </h1>
            <div class="ml12 aside-cta grid--cell print:d-none">

    <a href="{{route('questions.create')}}" class="ws-nowrap s-btn s-btn__primary" >
        Ask Question
    </a>

            </div>
        </div>
    @yield('content')
    
    <div class="pager fl">
        <span class="page-numbers current">1</span>
        <a href="/questions/tagged/laravel?tab=newest&page=2&pagesize=15" rel="" title="go to page 2"><span class="page-numbers">2</span></a>
        <a href="/questions/tagged/laravel?tab=newest&page=3&pagesize=15" rel="" title="go to page 3"><span class="page-numbers">3</span></a>
        <a href="/questions/tagged/laravel?tab=newest&page=4&pagesize=15" rel="" title="go to page 4"><span class="page-numbers">4</span></a>
        <a href="/questions/tagged/laravel?tab=newest&page=5&pagesize=15" rel="" title="go to page 5"><span class="page-numbers">5</span></a>
        <span class="page-numbers dots">…</span>
        <a href="/questions/tagged/laravel?tab=newest&page=9237&pagesize=15" rel="" title="go to page 9237"><span class="page-numbers">9237</span></a>
        <a href="/questions/tagged/laravel?tab=newest&page=2&pagesize=15" rel="next" title="go to page 2"><span class="page-numbers next"> next</span></a>
    </div>
</div>
</div>
</div>

<script src="{{asset('js/swal.min.js')}}"></script>
 
    
    <footer id="footer" class="site-footer js-footer" role="contentinfo">
        <div class="site-footer--container">       
            @include('user/footerkiri')
            @include('user/sosmed')
        </div>
    </footer>

    </body>
    </html>

    @stack('scripts')
