        <div id="questions" class="flush-left">
<div class="question-summary" id="question-summary-60429113">
    <div class="statscontainer">
        <div class="stats">
            <div class="vote">
                <div class="votes">
                    <span class="vote-count-post "><strong>0</strong></span>
                    <div class="viewcount">votes</div>
                </div>
            </div>
            <div class="status answered">
                <strong>1</strong>answer
            </div>
        </div>
        <div class="views " title="13 views">
    13 views
</div>
    </div>
    <div class="summary">
        <h3><a href="/questions/60429113/laravel-not-show-new-image-after-editing-only-if-remove-cache" class="question-hyperlink">laravel not show new image after editing only if remove cache</a></h3>
        <div class="excerpt">
            When I modify the images, the editing process succeeds, but no changes are made to the pictures and the new image does not appear after the edit except when I delete the cache
        </div>
        <div class="tags t-laravel t-laravel-5 t-eloquent">
            <a href="/questions/tagged/laravel" class="post-tag" title="show questions tagged &#39;laravel&#39;" rel="tag">laravel</a> <a href="/questions/tagged/laravel-5" class="post-tag" title="show questions tagged &#39;laravel-5&#39;" rel="tag">laravel-5</a> <a href="/questions/tagged/eloquent" class="post-tag" title="show questions tagged &#39;eloquent&#39;" rel="tag">eloquent</a> 
        </div>
        <div class="started fr">
            <div class="user-info ">
    <div class="user-action-time">
        asked <span title="2020-02-27 08:31:32Z" class="relativetime">25 mins ago</span>
    </div>
    <div class="user-gravatar32">
        <a href="/users/11005452/fedaa-elmasri"><div class="gravatar-wrapper-32"><img src="https://lh4.googleusercontent.com/-ZS2R6DGf_pQ/AAAAAAAAAAI/AAAAAAAAACI/LKbBFDV0MhM/photo.jpg?sz=32" alt="" width="32" height="32" class="bar-sm"></div></a>
    </div>
    
    <div class="user-details">
        <a href="/users/11005452/fedaa-elmasri">Fedaa Elmasri</a>
        <div class="-flair">
            <span class="reputation-score" title="reputation score " dir="ltr">23</span><span title="4 bronze badges" aria-hidden="true"><span class="badge3"></span><span class="badgecount">4</span></span><span class="v-visible-sr">4 bronze badges</span>
        </div>
    </div>
</div>
        </div>
    </div>

</div>