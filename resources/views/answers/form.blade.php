<div class="form-group {{ $errors->has('answer') ? 'has-error' : ''}}">
    <label for="answer" class="control-label">{{ 'Answer' }}</label>
    <textarea class="form-control" rows="5" name="answer" type="textarea" id="answer" >{{ isset($answer->answer) ? $answer->answer : ''}}</textarea>
    {!! $errors->first('answer', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
