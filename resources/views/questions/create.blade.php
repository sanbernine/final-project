<!DOCTYPE html>


    <html class="html__responsive html__unpinned-leftnav">

    <head>

        <title>Ask a Question - Stack Overflow</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://cdn.sstatic.net/Js/stub.en.js?v=2b995191bf00"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.sstatic.net/Shared/stacks.css?v=f6dc2e55f461" >
        <link rel="stylesheet" type="text/css" href="https://cdn.sstatic.net/Sites/stackoverflow/primary.css?v=387ed3d43d14" >
        <link rel="stylesheet" type="text/css" href="https://cdn.sstatic.net/Sites/stackoverflow/secondary.css?v=491798fe40f9" >

        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

        <script>
        StackExchange.init({"locale":"en","serverTime":1582802132,"routeName":"Questions/Ask","stackAuthUrl":"https://stackauth.com","networkMetaHostname":"meta.stackexchange.com","site":{"name":"Stack Overflow","description":"Q&A for professional and enthusiast programmers","isNoticesTabEnabled":true,"enableNewTagCreationWarning":true,"insertSpaceAfterNameTabCompletion":false,"id":1,"childUrl":"https://meta.stackoverflow.com","negativeVoteScoreFloor":null,"enableSocialMediaInSharePopup":true,"protocol":"https"},"user":{"fkey":"f48ce5e1d250738e4b068272932bc4b8aefb08653b2ac276c1f0329633ff5e47","tid":"f1ac7a8b-fc30-0951-5a87-edb8f337ae6a","rep":1,"isRegistered":true,"userType":3,"userId":12972716,"accountId":17858698,"gravatar":"<div class=\"gravatar-wrapper-32\"><img src=\"https://lh3.googleusercontent.com/a-/AAuE7mBTHLDQVesYd0BgZUBmI83knCSdSC8aj5_hqukPWw=k-s32\" alt=\"\" width=\"32\" height=\"32\" class=\"bar-sm\"></div>","profileUrl":"https://stackoverflow.com/users/12972716/haikal-shahab","canSeeDeletedPosts":false},"events":{"postType":{"question":1},"postEditionSection":{"title":1,"body":2,"tags":3}},"story":{"minCompleteBodyLength":75,"likedTagsMaxLength":300,"dislikedTagsMaxLength":300},"jobPreferences":{"maxNumDeveloperRoles":2,"maxNumIndustries":4},"svgIconPath":"https://cdn.sstatic.net/Img/svg-icons","svgIconHash":"c0e183a9e569"}, {"userProfile":{"openGraphAPIKey":"4a307e43-b625-49bb-af15-ffadf2bda017"},"userMessaging":{"showNewFeatureNotice":true},"tags":{},"snippets":{"renderDomain":"stacksnippets.net","snippetsEnabled":true},"slack":{"sidebarAdDismissCookie":"slack-sidebar-ad"},"site":{"allowImageUploads":true,"enableImgurHttps":true,"enableUserHovercards":true,"forceHttpsImages":true,"styleCode":true},"paths":{},"monitoring":{"clientTimingsAbsoluteTimeout":30000,"clientTimingsDebounceTimeout":1000},"mentions":{"maxNumUsersInDropdown":50},"markdown":{"asteriskIntraWordEmphasis":true},"flags":{"allowRetractingCommentFlags":true,"allowRetractingFlags":true},"comments":{},"accounts":{"currentPasswordRequiredForChangingStackIdPassword":true}});
        StackExchange.using.setCacheBreakers({"js/prettify-full.en.js":"14fe63213439","js/moderator.en.js":"05923ac0dadf","js/full-anon.en.js":"60eee770f07a","js/full.en.js":"a70c9cfd676e","js/wmd.en.js":"4031b207815c","js/mobile.en.js":"a168d277c579","js/help.en.js":"f9b7a69f17f2","js/tageditor.en.js":"1d9b6f166b48","js/tageditornew.en.js":"5924dca9bc36","js/inline-tag-editing.en.js":"4e684c7fc0bc","js/revisions.en.js":"9c63a754b3eb","js/review.en.js":"b9d45855b791","js/tagsuggestions.en.js":"9b2c5d9791d2","js/post-validation.en.js":"73b32222a9d5","js/explore-qlist.en.js":"8498d0bb288b","js/events.en.js":"baed9c11913d","js/keyboard-shortcuts.en.js":"8e4b1f347aab","js/adops.en.js":"22a9bd59b1e9","js/begin-edit-event.en.js":"7f52eac9bfd0","js/ask.en.js":"e4dd8c66240e","js/question-editor.en.js":"","js/snippet-javascript-codemirror.en.js":"a126c7a16bdd"});
        StackExchange.using("gps", function() {
             StackExchange.gps.init(true);
        });
    </script>

    </head>

<body class="ask-page unified-theme js-ask-page-v2 bg-black-050">
<header class="top-bar js-top-bar top-bar__network _fixed">
    <div class="wmx12 mx-auto grid ai-center h100" role="menubar">
        <div class="-main grid--cell">
                                <a href="{{route('questions.index')}}" class="-logo js-gps-track"
                        data-gps-track="top_nav.click({is_current:false, location:3, destination:8})">
                        <span class="-img _glyph">Stack Overflow</span>
                    </a>
        </div>

    </div>
</header>

    <script>
        StackExchange.ready(function () { StackExchange.topbar.init(); });
        StackExchange.scrollPadding.setPaddingTop(50, 10);     </script>

    <div class="container wmx12 bg-black-050">

        <div id="content" class="wmx12 bg-black-050 pt0 snippet-hidden">

<div class="grid ai-center py24 bg-no-repeat bg-right-bottom wide:bg-image-ask-v2 wide:h-ask-v2-background">
        <div class="fs-headline1">
            Create Question
        </div>
</div>

<div class="grid ai-start jc-space-between md:fd-column md:ai-stretch">
    <div class="grid--cell fl1 wmn0">
        
        <form id="post-form"
              class="post-form js-post-form"
              action="{{ route('questions.store') }}"
              method="post"
              >
                
                {{ csrf_field() }}
            <input type="hidden" name="qualityBanWarningShown" value="False" />
            <input type="hidden" name="priorAttemptCount" class="js-post-prior-attempt-count" value="0" /> 
            <input type="hidden" name="onboardingTemplate" />
            <input type="hidden" name="warntags" class="js-warned-tags-field" />
            <div id="question-form">
                <div class="bg-white bar-sm bs-md p16">
                    <div id="post-title" class="ps-relative mb16">
                        
                        <div class="grid fl1 fd-column js-stacks-validation">
                            <label class="d-block s-label mb4" for="title">
                                Judul
                                <p class="s-description mt2">Masukan Judul Question </p>
                            </label>
                            <div class="fl1 ps-relative">
                                <input id="title" name="title" type="text" maxlength="300" tabindex="100" placeholder="Masukan Judul Disini" class="s-input js-post-title-field" value="" data-min-length="15" data-max-length="150">
                            </div>
                            <div class="s-input-message mt4 d-none js-stacks-validation-message"></div>
                        </div>
                        <div id="question-suggestions" class="js-question-suggestions"></div>
                    </div>
<script>
    StackExchange.ready(function() {
        StackExchange.using("tagEditor", function () { StackExchange.tagEditor.ready.done(initFadingHelpText); });
        var channelOptions = {
            tags: "".split(" "),
            id: "1"
        };
        initTagRenderer("".split(" "), "".split(" "), channelOptions);
       
        StackExchange.using("externalEditor", function() {
            // Have to fire editor after snippets, if snippets enabled
            if (StackExchange.settings.snippets.snippetsEnabled) {
                StackExchange.using("snippets", function() {
                    createEditor();
                });
            }
            else {
                createEditor();
            }
        });

        function createEditor() {
            StackExchange.prepareEditor({
                heartbeatType: 'ask',
                autoActivateHeartbeat: false,
                convertImagesToLinks: true,
                noModals: true,
                showLowRepImageUploadWarning: true,
                reputationToPostImages: 10,
                bindNavPrevention: true,
                postfix: "",
                imageUploader: {
                brandingHtml: "Powered by \u003ca class=\"icon-imgur-white\" href=\"https://imgur.com/\"\u003e\u003c/a\u003e",
                    contentPolicyHtml: "User contributions licensed under \u003ca href=\"https://creativecommons.org/licenses/by-sa/4.0/\"\u003ecc by-sa 4.0 with attribution required\u003c/a\u003e \u003ca href=\"https://stackoverflow.com/legal/content-policy\"\u003e(content policy)\u003c/a\u003e",
                    allowUrls: true
                },
                onDemand: false,
                discardSelector: ".discard-question"
                ,immediatelyShowMarkdownHelp:true,userId:12972716
            });
            

        }
    });
</script>

<div id="post-editor" class="post-editor js-post-editor mt0 mb16">

    <div class="ps-relative">
        
            <label class="s-label mb4 d-block" for="wmd-input">
                Isi Question

                    <p class="s-description mt2">Masukan Pertanyaan Dengan Jelas</p>
            </label>
        <div class="wmd-container mb8">
            <div id="wmd-button-bar" class="wmd-button-bar btr-sm mt0"></div>
            <div class="js-stacks-validation">
                <div class="ps-relative">
                    <textarea id="wmd-input"
                              name="question"
                              class="wmd-input s-input bar0 js-post-body-field"
                              data-post-type-id="1"
                              cols="92" rows="15"
                              tabindex="101"
                              data-min-length=""></textarea>
                </div>
                <div class="s-input-message mt4 d-none js-stacks-validation-message"></div>
            </div>
        </div>
    </div>
    <div id="draft-saved" class="fc-success float-left h24" style="display:none;">Draft saved</div>
    <div id="draft-discarded" class="fc-error float-left h24" style="display:none;">Draft discarded</div>
        <div id="wmd-preview" class="wmd-preview" ></div>
</div>
<div class="ps-relative" id="tag-editor">
<div class="ps-relative">
    
    <div class="form-item p0 js-stacks-validation js-tag-editor-container">
        <div class="grid ai-center jc-space-between">
            <label for="tag" class="s-label mb4 d-block grid--cell fl1">
                Tags
                    
            </label>
              
        </div>
        <div class="ps-relative">
            <input id="tag" data-role="tagsinput"  name="tags" type="text"  value="" >
        </div>
        
    </div>
</div>
                            <div class="js-tag-suggestions hmn0"></div>
                    </div>
                </div>
            </div>
            <div class="js-visible-before-review">
                <div class="grid gsx gs4 ai-center mt32">
                    <input type="submit" class="btn btn-primary" value="Create">
                    <button class="grid--cell s-btn s-btn__danger ws-nowrap discard-question dno">Discard draft</button>
                </div>
            </div>
           
        </form>
    </div>
 
</div>

        </div>
    </div>

<script>
  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/bootstrap-tagsinput.min.js')}}"></script>
</script>

    </body>
    </html>

