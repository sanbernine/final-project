@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Question {{ $question->id }}</div>
                    <div class="card-body">

                        <a href="{{ route('questions.index') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ route('questions.edit', $question->id) }}" title="Edit Question"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ route('questions.destroy', $question->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm" title="Delete Question" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $question->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Title </th><td> {{ $question->title }} </td>
                                    </tr>
                                    <tr>
                                        <th> Question </th><td> {{ $question->question }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <form method="POST" action="{{ route('questions.vote', ['question' => $question->id, 'vote' => 'up']) }}" accept-charset="UTF-8" style="display:inline">
                            @csrf
                            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i> Vote Up</button>
                        </form>

                        <form method="POST" action="{{ route('questions.vote', ['question' => $question->id, 'vote' => 'down']) }}" accept-charset="UTF-8" style="display:inline">
                            @csrf
                            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i> Vote Down</button>
                        </form>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Qcomments</div>
                    <div class="card-body">
                        <a href="{{ route('question.comments.create', $question->id) }}" class="btn btn-success btn-sm" title="Add New QComment">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Comment</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($qcomments as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->comment }}</td>
                                        <td>
                                            <a href="{{ route('question.comments.show', ['question' => $question->id, 'comment' => $item->id]) }}" title="View QComment"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ route('question.comments.edit', ['question' => $question->id, 'comment' => $item->id]) }}" title="Edit QComment"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ route('question.comments.destroy', ['question' => $question->id, 'comment' => $item->id]) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete QComment" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $qcomments->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Answers</div>
                    <div class="card-body">
                        <a href="{{ route('answers.create', $question->id) }}" class="btn btn-success btn-sm" title="Add New Answer">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Answer</th><th>Is Accepted</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($answers as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->answer }}</td><td>{{ $item->is_accepted }}</td>
                                        <td>
                                            <form method="POST" action="{{ route('answers.accept', ['question' => $question->id, 'answer' => $item->id]) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i> Pilih Jawaban</button>
                                            </form>

                                            <form method="POST" action="{{ route('answers.vote', ['question' => $question->id, 'answer' => $item->id, 'vote' => 'up']) }}" accept-charset="UTF-8" style="display:inline">
                                                @csrf
                                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i> Vote Up</button>
                                            </form>
                    
                                            <form method="POST" action="{{ route('answers.vote', ['question' => $question->id, 'answer' => $item->id, 'vote' => 'down']) }}" accept-charset="UTF-8" style="display:inline">
                                                @csrf
                                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i> Vote Down</button>
                                            </form>

                                            <a href="{{ route('answers.show', ['question' => $question->id, 'answer' => $item->id]) }}" title="View Answer"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            
                                            <a href="{{ route('answers.edit', ['question' => $question->id, 'answer' => $item->id]) }}" title="Edit Answer"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ route('answers.destroy', ['question' => $question->id, 'answer' => $item->id]) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Answer" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $answers->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
