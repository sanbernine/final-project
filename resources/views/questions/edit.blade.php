<!DOCTYPE html>


    <html itemscope itemtype="http://schema.org/QAPage" class="html__responsive">

    <head>

        <title>Stack Overflow Sanbernine</title>
        <link rel="shortcut icon" href="https://cdn.sstatic.net/Sites/stackoverflow/img/favicon.ico?v=4f32ecc8f43d">
        <link rel="apple-touch-icon" href="https://cdn.sstatic.net/Sites/stackoverflow/img/apple-touch-icon.png?v=c78bd457575a">
        <link rel="image_src" href="https://cdn.sstatic.net/Sites/stackoverflow/img/apple-touch-icon.png?v=c78bd457575a"> 
        <link rel="search" type="application/opensearchdescription+xml" title="Stack Overflow" href="/opensearch.xml">
        <link rel="canonical" href="https://stackoverflow.com/questions/50639861/append-messages-to-push-notification-firebase" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://cdn.sstatic.net/Js/stub.en.js?v=2b995191bf00"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.sstatic.net/Shared/stacks.css?v=f6dc2e55f461" >
        <link rel="stylesheet" type="text/css" href="https://cdn.sstatic.net/Sites/stackoverflow/primary.css?v=3b9bab7da4ab" >
        <link rel="alternate" type="application/atom+xml" title="Feed for question &#39;Append messages to push notification - Firebase&#39;" href="/feeds/question/50639861">
    </head>

        <body class="question-page unified-theme">
    <div id="notify-container"></div>
    <div id="custom-header"></div>
<header class="top-bar js-top-bar top-bar__network _fixed">
    <div class="wmx12 mx-auto grid ai-center h100" role="menubar">
        <div class="-main grid--cell">
                <a href="#" class="left-sidebar-toggle p0 ai-center jc-center js-left-sidebar-toggle" role="menuitem" aria-haspopup="true" aria-controls="left-sidebar" aria-expanded="false"><span class="ps-relative"></span>
                </a>
                    <div class="topbar-dialog leftnav-dialog js-leftnav-dialog dno">
                        <div class="left-sidebar js-unpinned-left-sidebar" data-can-be="left-sidebar" data-is-here-when="sm"></div>
                    </div>
                <a href="{{route('questions.index')}}" class="-logo js-gps-track"
                        data-gps-track="top_nav.click({is_current:false, location:2, destination:8})">
                        <span class="-img _glyph">Stack Overflow</span>
                </a>
        </div>
    </div>
</header>

  

    <div class="container">
<div id="left-sidebar" data-is-here-when="md lg" class="left-sidebar js-pinned-left-sidebar ps-relative">
</div>
        <div id="content" class="snippet-hidden">

<div itemprop="mainEntity" itemscope itemtype="http://schema.org/Question">
    <link itemprop="image" href="https://cdn.sstatic.net/Sites/stackoverflow/img/apple-touch-icon.png?v=c78bd457575a">

    <div class="inner-content clearfix">

                            <div id="question-header" class="grid sm:fd-column">
                                <h1 itemprop="name" class="grid--cell fs-headline1 fl1 ow-break-word mb8"><a href="/questions/50639861/append-messages-to-push-notification-firebase" class="question-hyperlink">{{$question->title}}</a></h1>
                        <div class="ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end">
                        </div>
                    </div>
            <div class="grid fw-wrap pb8 mb16 bb bc-black-2">
                    <div class="grid--cell ws-nowrap mr16 mb8" title="2018-06-01 09:07:50Z">
                        <span class="fc-light mr2">Asked</span>
                        <time itemprop="dateCreated" datetime="2018-06-01T09:07:50">{{$question->created_at}}</time>
                    </div>
                        <div class="grid--cell ws-nowrap mr16 mb8">
                            <span class="fc-light mr2">Active</span>
                            <a href="?lastactivity" class="s-link s-link__inherit" title="2019-01-15 06:42:43Z">1 year, 1 month ago</a>
                        </div>
                    <div class="grid--cell ws-nowrap mb8" title="Viewed 209 times">
                        <span class="fc-light mr2">Viewed</span>
                        209 times
                    </div>
            </div>
                    <div id="mainbar" role="main" aria-label="question and answers">
                

<div class="question" data-questionid="50639861"  id="question">

    <style>.everyoneloves__top-leaderboard:empty,.everyoneloves__mid-leaderboard:empty,.everyoneloves__bot-mid-leaderboard:empty{
            margin-bottom:0;
}
</style>

<div id="clc-tlb" class="everyonelovesstackoverflow everyoneloves__top-leaderboard"></div>
    <div class="post-layout">
        <div class="votecell post-layout--left">
<div class="js-voting-container grid fd-column ai-stretch gs4 fc-black-200" data-post-id="50639861">
        <button class="js-vote-up-btn grid--cell s-btn s-btn__unset c-pointer" title="This question shows research effort; it is useful and clear (click again to undo)" aria-pressed="false" aria-label="up vote" data-selected-classes="fc-theme-primary"><svg aria-hidden="true" class="svg-icon m0 iconArrowUpLg" width="36" height="36" viewBox="0 0 36 36"><path d="M2 26h32L18 10 2 26z"/></svg></button>
        <div class="js-vote-count grid--cell fc-black-500 fs-title grid fd-column ai-center" itemprop="upvoteCount" data-value="0">0</div>
        <button class="js-vote-down-btn grid--cell s-btn s-btn__unset c-pointer" title="This question does not show any research effort; it is unclear or not useful (click again to undo)" aria-pressed="false" aria-label="down vote" data-selected-classes="fc-theme-primary"><svg aria-hidden="true" class="svg-icon m0 iconArrowDownLg" width="36" height="36" viewBox="0 0 36 36"><path d="M2 10h32L18 26 2 10z"/></svg></button>

        <button class="js-favorite-btn s-btn s-btn__unset c-pointer py4 js-gps-track" aria-pressed="false" aria-label="favorite (1)" data-selected-classes="fc-yellow-600"
                data-gps-track="post.click({ item: 1, priv: -1, post_type: 1 })">
            <svg aria-hidden="true" class="svg-icon iconStar" width="18" height="18" viewBox="0 0 18 18"><path d="M9 12.65l-5.29 3.63 1.82-6.15L.44 6.22l6.42-.17L9 0l2.14 6.05 6.42.17-5.1 3.9 1.83 6.16L9 12.65z"/></svg>
            <div class="js-favorite-count mt4" data-value="1"></div>
        </button>
        
        <a class="js-post-issue grid--cell s-btn s-btn__unset c-pointer py8 mx-auto" href="/posts/50639861/timeline" data-shortcut="T" title="Timeline"><svg aria-hidden="true" class="svg-icon mln2 mr0 iconHistory" width="19" height="18" viewBox="0 0 19 18"><path d="M3 9a8 8 0 1 1 3.73 6.77L8.2 14.3A6 6 0 1 0 5 9l3.01-.01-4 4-4-4h3L3 9zm7-4h1.01L11 9.36l3.22 2.1-.6.93L10 10V5z"/></svg></a>
</div>
        </div>
            <div class="postcell post-layout--right">
                
                @include('layouts.isi')
            </div>
    <div class="post-layout--right">
        <div id="comments-50639861" class="comments js-comments-container bt bc-black-2 mt12 " data-post-id="50639861" data-min-length="15">
            <ul class="comments-list js-comments-list"
                    data-remaining-comments-count="0"
                    data-canpost="false"
                    data-cansee="true"
                    data-comments-unavailable="false"
                    data-addlink-disabled="true">
    @include('layouts.tampilquestion')
            </ul>
        </div>
        <div id="comments-link-50639861" data-rep=50 data-reg=true>

                    <a  title="Use comments to ask for more information or suggest improvements. Avoid answering questions in comments."  href="{{ route('question.comments.create', $question->id) }}" role="button">add a comment question</a>

                <span class="js-link-separator dno">&nbsp;|&nbsp;</span>
            <a class="js-show-link comments-link dno" title="expand to show all comments on this post" href=# onclick="" role="button"></a>
        </div>         
    </div>            
</div>
</div>


            <!-- Answer -->

                <div id="answers">

                    <a name="tab-top"></a>
                    <div id="answers-header">
                        <div class="subheader answers-subheader">
                            <h2 data-answercount="1">
                                    Answer
                                <span style="display:none;" itemprop="answerCount">1</span>
                            </h2>
                            <div>
                    

                            </div>
                        </div>
                    </div>


<!-- answer -->
@foreach($answers as $item)
<div class="question" data-questionid="50639861"  id="question">

    <style>.everyoneloves__top-leaderboard:empty,.everyoneloves__mid-leaderboard:empty,.everyoneloves__bot-mid-leaderboard:empty{
            margin-bottom:0;
}
</style>
<div id="clc-tlb" class="everyonelovesstackoverflow everyoneloves__top-leaderboard"></div>
    <div class="post-layout">
        <div class="votecell post-layout--left">
<div class="js-voting-container grid fd-column ai-stretch gs4 fc-black-200" data-post-id="50639861">
        <button class="js-vote-up-btn grid--cell s-btn s-btn__unset c-pointer" title="This question shows research effort; it is useful and clear (click again to undo)" aria-pressed="false" aria-label="up vote" data-selected-classes="fc-theme-primary"><svg aria-hidden="true" class="svg-icon m0 iconArrowUpLg" width="36" height="36" viewBox="0 0 36 36"><path d="M2 26h32L18 10 2 26z"/></svg></button>
        <div class="js-vote-count grid--cell fc-black-500 fs-title grid fd-column ai-center" itemprop="upvoteCount" data-value="0">0</div>
        <button class="js-vote-down-btn grid--cell s-btn s-btn__unset c-pointer" title="This question does not show any research effort; it is unclear or not useful (click again to undo)" aria-pressed="false" aria-label="down vote" data-selected-classes="fc-theme-primary"><svg aria-hidden="true" class="svg-icon m0 iconArrowDownLg" width="36" height="36" viewBox="0 0 36 36"><path d="M2 10h32L18 26 2 10z"/></svg></button>

        <button class="js-favorite-btn s-btn s-btn__unset c-pointer py4 js-gps-track" aria-pressed="false" aria-label="favorite (1)" data-selected-classes="fc-yellow-600"
                data-gps-track="post.click({ item: 1, priv: -1, post_type: 1 })">
            <svg aria-hidden="true" class="svg-icon iconStar" width="18" height="18" viewBox="0 0 18 18"><path d="M9 12.65l-5.29 3.63 1.82-6.15L.44 6.22l6.42-.17L9 0l2.14 6.05 6.42.17-5.1 3.9 1.83 6.16L9 12.65z"/></svg>
            <div class="js-favorite-count mt4" data-value="1">1</div>
        </button>
    

    
        <a class="js-post-issue grid--cell s-btn s-btn__unset c-pointer py8 mx-auto" href="/posts/50639861/timeline" data-shortcut="T" title="Timeline"><svg aria-hidden="true" class="svg-icon mln2 mr0 iconHistory" width="19" height="18" viewBox="0 0 19 18"><path d="M3 9a8 8 0 1 1 3.73 6.77L8.2 14.3A6 6 0 1 0 5 9l3.01-.01-4 4-4-4h3L3 9zm7-4h1.01L11 9.36l3.22 2.1-.6.93L10 10V5z"/></svg></a>
</div>
        </div>
            <div class="postcell post-layout--right">
                
                @include('layouts.isianswer')
            </div>
    <!-- question answer -->
    <div class="post-layout--right">
        <div id="comments-50639861" class="comments js-comments-container bt bc-black-2 mt12 " data-post-id="50639861" data-min-length="15">
            <ul class="comments-list js-comments-list"
                    data-remaining-comments-count="0"
                    data-canpost="false"
                    data-cansee="true"
                    data-comments-unavailable="false"
                    data-addlink-disabled="true">
    @include('layouts.tampilquestionanswer')
            </ul>
            <div id="comments-link-50639861" data-rep=50 data-reg=true>

                    <a  title="Use comments to ask for more information or suggest improvements. Avoid answering questions in comments."  href="{{ route('answer.comments.create', ['question' => $question->id, 'answer' => $item->id]) }}" role="button">add a comment Answer</a>

                <span class="js-link-separator dno">&nbsp;|&nbsp;</span>
            <a class="js-show-link comments-link dno" title="expand to show all comments on this post" href=# onclick="" role="button"></a>
        </div>
        </div>
                 
    </div> 
    <!-- /questionanswer -->

</div>
</div>
@endforeach

  

<div id="answer-50650018" class="answer accepted-answer" data-answerid="50650018"  itemprop="acceptedAnswer" itemscope itemtype="http://schema.org/Answer">
    <div class="post-layout">
        <div class="votecell post-layout--left">
            


        </div>

    <div class="post-layout--right">
       

        <div id="comments-link-50650018" data-rep=50 data-reg=true>

                    <a  title="Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”."  href="{{ route('answers.create', $question->id) }}" role="button">Tambah Jawaban</a>
                <span class="js-link-separator dno">&nbsp;|&nbsp;</span>
            <a class="js-show-link comments-link dno" title="expand to show all comments on this post" href=# onclick="" role="button"></a>
        </div>  

    </div>    </div>
</div>

                        
                            <form id="post-form" action="/questions/50639861/answer/submit" method="post" class="js-add-answer-component post-form">
                                <input type="hidden" id="post-id" value="50639861" />
                                <input type="hidden" id="qualityBanWarningShown" name="qualityBanWarningShown" value="false" />
                                <input type="hidden" name="referrer" value="https://stackoverflow.com/questions/60424804/call-to-undefined-method-illuminate-database-eloquent-relations-belongstomanyr" />
                                <h2 class="space">
                                    Your Answer
                                </h2>
<div id="post-editor" class="post-editor js-post-editor">
    <div class="ps-relative">
        <div class="wmd-container mb8">
            <div id="wmd-button-bar" class="wmd-button-bar btr-sm"></div>
            <div class="js-stacks-validation">
                <div class="ps-relative">
                    <textarea id="wmd-input"
                              name="post-text"
                              class="wmd-input s-input bar0 js-post-body-field"
                              data-post-type-id="2"
                              cols="92" rows="15"
                              tabindex="101"
                              data-min-length=""></textarea>
                </div>
                <div class="s-input-message mt4 d-none js-stacks-validation-message"></div>
            </div>
        </div>
    </div>
    <aside class="grid ai-start jc-space-between js-answer-help s-notice s-notice__warning pb0 pr4 pt4 mb8 d-none" role="status" aria-hidden="true">
    <div class="grid--cell pt8">
        <p>Thanks for contributing an answer to Stack Overflow!</p><ul><li>Please be sure to <em>answer the question</em>. Provide details and share your research!</li></ul><p>But <em>avoid</em> …</p><ul><li>Asking for help, clarification, or responding to other answers.</li><li>Making statements based on opinion; back them up with references or personal experience.</li></ul><p>To learn more, see our <a href="/help/how-to-answer">tips on writing great answers</a>.</p>
    </div>
    <button class="grid--cell js-answer-help-close-btn s-btn s-btn__muted fc-dark">
        <svg aria-hidden="true" class="svg-icon iconClear" width="18" height="18" viewBox="0 0 18 18"><path d="M15 4.41L13.59 3 9 7.59 4.41 3 3 4.41 7.59 9 3 13.59 4.41 15 9 10.41 13.59 15 15 13.59 10.41 9 15 4.41z"/></svg>
    </button>
</aside>



    <div id="draft-saved" class="fc-success float-left h24" style="display:none;">Draft saved</div>
    <div id="draft-discarded" class="fc-error float-left h24" style="display:none;">Draft discarded</div>



        <div id="wmd-preview" class="wmd-preview" ></div>
        <div></div>
            <div class="edit-block">
            <input id="fkey" name="fkey" type="hidden" value="df1c71bb18b9a2980cbf02ca227398e197f2c2f529341b7590cc09a28da03c9e">
            <input id="author" name="author" type="text">
        </div>

</div>

                                <div class="ps-relative">
                                    
                                    
                                </div>

                        <div class="form-submit cbt grid gsx gs4">
                                        <button id="submit-button" class="grid--cell s-btn s-btn__primary s-btn__icon" type="submit" tabindex="120" autocomplete="off">
                                                    Post Your Answer                                        </button>
                                        <button class="grid--cell s-btn s-btn__danger discard-answer dno">
                                            Discard
                                        </button>
                                    </div>
                                    <div class="js-general-error general-error cbt d-none"></div>
                            </form>
                </div>
            </div>
    </div>
</div>
    <script>
        $('#wmd-input').one("keypress", function () {
            $.ajax({
                url: '/accounts/email-settings-form',
                cache: false,
                success: function (data) {
                    $('#submit-button').parent().prepend(data);
                }
            });
        });

    </script>

        </div>
    </div>
    
    @include('admin.footer') 
    </body>
    </html>
