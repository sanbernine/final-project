@push('style')
    <link href="{{ asset('css/simplemde.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-tagsinput.css') }}" rel="stylesheet">
@endpush

<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ isset($question->title) ? $question->title : ''}}" >
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('question') ? 'has-error' : ''}}">
    <label for="question" class="control-label">{{ 'Question' }}</label>
    <textarea class="form-control" rows="5" name="question" type="textarea" id="question" >{{ isset($question->question) ? $question->question : ''}}</textarea>
    {!! $errors->first('question', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('tags') ? 'has-error' : ''}}">
    <label for="tags" class="control-label">{{ 'Tags' }}</label>
    <input class="form-control" name="tags" type="text" id="tags" value="{{ isset($question->tags) ? $question->tags : ''}}" data-role="tagsinput">
    {!! $errors->first('tags', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

@push('scripts')
    <script src="{{ asset('js/simplemde.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>

    <script>
        var simplemde = new SimpleMDE({ element: document.getElementById("question") });
    </script>
@endpush