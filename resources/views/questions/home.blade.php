@extends('layouts.coba')

@section('content')
<div id="qlist-wrapper" class="flush-left">
    <div id="question-mini-list">
        <div>

<div class="question-summary narrow"
     id="question-summary-60443262">
    <div onclick="window.location.href='/questions/60443262/trying-to-use-cyl-bessel-i-from-std-c'" class="cp">
        <div class="votes">
            <div class="mini-counts"><span title="0 votes">0</span></div>
            <div>votes</div>
        </div>
        <div class="status unanswered">
            <div class="mini-counts"><span title="0 answers">0</span></div>
            <div>answers</div>
        </div>
        <div class="views">
            <div class="mini-counts"><span title="6 views">6</span></div>
            <div>views</div>
        </div>
    </div>
    <div class="summary">
        
        
        <h3><a href="/questions/60443262/trying-to-use-cyl-bessel-i-from-std-c" class="question-hyperlink">Trying to use &#39;cyl_bessel_i&#39; from STD C++</a></h3>
        <div class="tags t-c&#231;&#231; t-c&#231;&#231;17">
            <a href="/questions/tagged/c%2b%2b" class="post-tag" title="show questions tagged &#39;c++&#39;" rel="tag">c++</a> <a href="/questions/tagged/c%2b%2b17" class="post-tag" title="show questions tagged &#39;c++17&#39;" rel="tag">c++17</a> 
        </div>
        <div class="started">
            <a href="/questions/60443262/trying-to-use-cyl-bessel-i-from-std-c" class="started-link">modified <span title="2020-02-27 23:52:36Z" class="relativetime">1 min ago</span></a>
            <a href="/users/1427563/jla">jla</a> <span class="reputation-score" title="reputation score " dir="ltr">2,281</span>
        </div>
    </div>
</div>

  </div>
    </div>
</div>
@endsection